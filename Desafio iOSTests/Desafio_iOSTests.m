//
//  Desafio_iOSTests.m
//  Desafio iOSTests
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CSOSearchEngine.h"
#import "CSORepositoryEngine.h"
#import "CSOUser+UnitTests.h"
#import "CSOPullRequest+UnitTests.h"
#import "CSORepository+UnitTests.h"

@interface Desafio_iOSTests : XCTestCase

@property (strong, nonatomic) NSDictionary * userMockup ;

@end

@implementation Desafio_iOSTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    _userMockup = @{@"id":@123456,@"login":@"user",@"avatar_url":@"http://www.google.com"};

}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

-(void) check {
    
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

#pragma mark - Unit Tests
// Unit Test: CSOUser model
-(void) testUserModel {
    
    
    CSOUser * user = [CSOUser initWithJson:_userMockup];
    
    NSString * testResultMessage = [user testAttributes];
    BOOL wasInitiated = testResultMessage == nil ;
    
    XCTAssertTrue(wasInitiated,@"'%@' wasn't initiated properly.",testResultMessage);
    
}

// Unit Test: CSOPullRequest model
-(void) testPullRequestModel {
    
    NSDictionary * mockupJson = @{@"id":@1234,@"title":@"This is a test pull.",@"body":@"body",@"created_at":@"2016-10-10T10:16:57Z",@"user":_userMockup, @"url":@"http://www.google.com"};
    
    CSOPullRequest * pull = [CSOPullRequest initWithJson:mockupJson];
    
    NSString * testResultMessage = [pull testAttributes];
    BOOL wasInitiated = testResultMessage == nil ;
    
    XCTAssertTrue(wasInitiated,@"'%@' wasn't initiated properly.",testResultMessage);
    
}

// Unit Test: CSORepository model
-(void) testRepositoryModel {
    
    NSDictionary * mockupJson = @{@"id":@1234,@"name":@"RepName",@"description":@"details",@"url":@"http://www.google.com",@"forks_count":@2,@"stargazers_count":@5,@"owner":_userMockup};
    
    CSORepository * rep = [CSORepository initWithJson:mockupJson];
    NSString * testResultMessage = [rep testAttributes];
    
    BOOL wasInitiated = testResultMessage == nil ;
    
    XCTAssertTrue(wasInitiated,@"'%@' wasn't initiated properly.",testResultMessage);
}

- (void) testRepositoryApi {
    XCTestExpectation * exp = [self expectationWithDescription:@"Calls api. Should return array of CSOPullRequests"];
    
    [[CSORepositoryEngine sharedEngine] getPullsWith:@"facebook" andRepository:@"react-native" withCompletion:^(NSArray * parsedItems, NSError * error, NSDictionary * pagination) {
        if (error) {
            XCTFail(@"Timeout error: %@", error.description);
        }
        
        if (!parsedItems || parsedItems.count == 0) {
            XCTFail(@"CSOPullRequest array not initialized");
        }
        
        for (id object in parsedItems) {
            if (![object isKindOfClass:[CSOPullRequest class]]) {
                XCTFail(@"Not all items are CSOPullRequest instances");
            }
        }
        
        XCTAssertTrue(YES);
        [exp fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError * _Nullable error) {
        if (error) {
            XCTFail(@"waitForExpectationsWithTimeout errored: %@", error.description);
        }
    }];
}

- (void) testSearchApi {
    XCTestExpectation * exp = [self expectationWithDescription:@"Calls api. Should return array of CSORepositories"];
    
    [[CSOSearchEngine sharedEngine] searchMostPopularJavaRepositoriesInPage:1 withCompletion:^(NSArray * parsedItems, NSError *error, NSDictionary * pagination) {
        if (error) {
            XCTFail(@"Timeout error: %@", error.description);
        }
        
        if (!parsedItems || parsedItems.count == 0) {
            XCTFail(@"CSORepository array not initialized");
        }
        
        for (id object in parsedItems) {
            if (![object isKindOfClass:[CSORepository class]]) {
                XCTFail(@"Not all items are CSORepository instances");
            }
        }
        
        XCTAssertTrue(YES);
        [exp fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:10 handler:^(NSError * _Nullable error) {
        if (error) {
            XCTFail(@"waitForExpectationsWithTimeout errored: %@", error.description);
        }
    }];
}

@end
