//
//  CSOPullRequest+UnitTests.h
//  Desafio iOS
//
//  Created by Diego Varela on 10/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOPullRequest.h"

@interface CSOPullRequest (UnitTests)

-(NSString *) testAttributes ;

@end
