//
//  CSORepository+UnitTests.m
//  Desafio iOS
//
//  Created by Diego Varela on 10/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepository+UnitTests.h"
#import "CSOUser+UnitTests.h"

@implementation CSORepository (UnitTests)

-(NSString *)testAttributes {
    NSString * attribute = nil ;
    
    
    if (self._id == nil || ![self._id isKindOfClass:[NSNumber class]]) {
        attribute = @"_id" ;
    }
    
    if (self.JSON == nil || ![self.JSON isKindOfClass:[NSDictionary class]]) {
        attribute = @"JSON" ;
    }
    
    if (self.name == nil || ![self.name isKindOfClass:[NSString class]]) {
        attribute = @"name" ;
    }
    
    if (self.details == nil || ![self.details isKindOfClass:[NSString class]]) {
        attribute = @"details" ;
    }
    
    if (self.url == nil || ![self.url isKindOfClass:[NSString class]]) {
        attribute = @"url" ;
    }
    
    if (self.forks == nil || ![self.forks isKindOfClass:[NSNumber class]]) {
        attribute = @"forks" ;
    }
    
    if (self.stars == nil || ![self.stars isKindOfClass:[NSNumber class]]) {
        attribute = @"stars" ;
    }
    
    NSString * errorAtt = [self.owner testAttributes];
    
    if (errorAtt) {
        return [@"owner." stringByAppendingString:errorAtt];
    }
    
    return attribute ;
}


@end
