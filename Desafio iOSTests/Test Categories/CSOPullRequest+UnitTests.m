//
//  CSOPullRequest+UnitTests.m
//  Desafio iOS
//
//  Created by Diego Varela on 10/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOPullRequest+UnitTests.h"
#import "CSOUser+UnitTests.h"

@implementation CSOPullRequest (UnitTests)

-(NSString *)testAttributes {
    NSString * attribute = nil ;
    
    
    if (self._id == nil || ![self._id isKindOfClass:[NSNumber class]]) {
        attribute = @"_id" ;
    }
    
    if (self.JSON == nil || ![self.JSON isKindOfClass:[NSDictionary class]]) {
        attribute = @"JSON" ;
    }
    
    if (self.title == nil || ![self.title isKindOfClass:[NSString class]]) {
        attribute = @"login" ;
    }
    
    if (self.body == nil || ![self.body isKindOfClass:[NSString class]]) {
        attribute = @"body" ;
    }
    
    if (self.created == nil || ![self.created isKindOfClass:[NSDate class]]) {
        attribute = @"created" ;
    }
    
    if (self.url == nil || ![self.url isKindOfClass:[NSString class]]) {
        attribute = @"url" ;
    }
    
    NSString * errorAtt = [self.user testAttributes];
    
    if (errorAtt) {
        return [@"user." stringByAppendingString:errorAtt];
    }
    
    return attribute ;
}

@end
