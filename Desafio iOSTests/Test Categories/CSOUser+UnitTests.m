//
//  CSOUser+UnitTests.m
//  Desafio iOS
//
//  Created by Diego Varela on 10/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOUser+UnitTests.h"

@implementation CSOUser (UnitTests)

-(NSString *) testAttributes {
    
    NSString * attribute = nil ;
    
    
    if (self._id == nil || ![self._id isKindOfClass:[NSNumber class]]) {
        attribute = @"_id" ;
    }
    
    if (self.JSON == nil || ![self.JSON isKindOfClass:[NSDictionary class]]) {
        attribute = @"JSON" ;
    }
    
    if (self.login == nil || ![self.login isKindOfClass:[NSString class]]) {
        attribute = @"login" ;
    }
    
    if (self.avatarUrl == nil || ![self.avatarUrl isKindOfClass:[NSString class]]) {
        attribute = @"avatarUrl" ;
    }
    
    return attribute ;
}

@end
