//
//  CSOPullRequestCell.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOPullRequestCell.h"
#import "UIImageView+WebCache.h"

@implementation CSOPullRequestCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void) fillDataWith:(CSOPullRequest *) pull {
    self.titleLabel.text = pull.title ;
    self.bodyLabel.text = pull.body ;
    self.usernameLabel.text = pull.user.login ;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:pull.user.avatarUrl]];
}

@end
