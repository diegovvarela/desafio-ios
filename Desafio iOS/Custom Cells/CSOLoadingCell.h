//
//  CSOLoadingCell.h
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CSOLoadingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UILabel *loadingLabel;

@end
