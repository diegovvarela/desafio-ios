//
//  CSOPullRequestCell.h
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSOPullRequest.h"

@interface CSOPullRequestCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *bodyLabel;

-(void) fillDataWith:(CSOPullRequest *) pull ;

@end
