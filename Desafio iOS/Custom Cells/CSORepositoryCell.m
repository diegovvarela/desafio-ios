//
//  CSORepositoryCell.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepositoryCell.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+TintColor.h"

@implementation CSORepositoryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.starsImageView setImageRenderingWithTintColor:[UIImage imageNamed:@"star"]];
    [self.forksImageView setImageRenderingWithTintColor:[UIImage imageNamed:@"fork"]];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)layoutSubviews {
//    self.avatarImageView.layer.cornerRadius = self.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClassRegular ? 36.f : 18.f  ;
//}

-(void) fillDataWith:(CSORepository *) repository {
    self.nameLabel.text = repository.name ;
    self.detailsLabel.text = repository.details ;
    self.starsLabel.text = [NSString stringWithFormat:@"%d",repository.stars.intValue];
    self.forksLabel.text = [NSString stringWithFormat:@"%d",repository.forks.intValue];
    self.usernameLabel.text = repository.owner.login ;
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:repository.owner.avatarUrl]];
    
}

@end
