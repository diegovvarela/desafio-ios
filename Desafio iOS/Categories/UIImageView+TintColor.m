//
//  UIImageView+TintColor.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "UIImageView+TintColor.h"

@implementation UIImageView (TintColor)

-(void) setImageRenderingWithTintColor:(UIImage *) image {
    self.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
