//
//  UIAlertView+Errors.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "UIAlertView+Errors.h"

@implementation UIAlertView (Errors)

+(void) showCustomErrorAlert {
    
    [[[UIAlertView alloc] initWithTitle:@"Hey!" message:@"Something went wrong... Please check your conection, and try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
