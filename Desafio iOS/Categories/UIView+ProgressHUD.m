//
//  UIView+ProgressHUD.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "UIView+ProgressHUD.h"

@implementation UIView (ProgressHUD)

-(MBProgressHUD *)addProgressHUDWithTitle:(NSString *)title {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.backgroundColor = [UIColor clearColor];
    hud.label.text = title;
    
    return hud ;
}

@end
