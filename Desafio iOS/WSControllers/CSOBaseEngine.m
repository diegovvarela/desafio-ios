//
//  CSOBaseEngine.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOBaseEngine.h"
#import "AFNetworking.h"

static NSString * baseUrl = @"https://api.github.com/" ;

@implementation CSOBaseEngine

-(void) makeRequestWithApiPath:(NSString *) path requestType:(RequestType) requestType parameters:(NSDictionary *) parameters withCompletion:(CSOBaseEngineCompletion) completion {
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    NSString * url = [baseUrl stringByAppendingString:path];
    
    switch (requestType) {
        case RequestTypeGet:{
            
            [manager GET:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                completion(responseObject,nil,nil);
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                completion(nil, error,nil);
            }];
            
            break;
        }
            
        case RequestTypePost: {
            
            [manager POST:url parameters:parameters progress:nil success:^(NSURLSessionTask *task, id responseObject) {
                completion(responseObject,nil,nil);
            } failure:^(NSURLSessionTask *operation, NSError *error) {
                completion(nil, error, nil);
            }];
            
            break;
        }
            
        default:
            break;
    }
}

@end
