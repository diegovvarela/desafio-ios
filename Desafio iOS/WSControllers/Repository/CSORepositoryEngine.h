//
//  CSORepositoryEngine.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOBaseEngine.h"

@interface CSORepositoryEngine : CSOBaseEngine

+(instancetype) sharedEngine ;


-(void) getPullsWith:(NSString *) ownerName andRepository:(NSString *) repositoryName withCompletion:(CSOBaseEngineCompletion) completion ;

@end
