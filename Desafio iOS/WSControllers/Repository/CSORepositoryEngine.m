//
//  CSORepositoryEngine.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepositoryEngine.h"
#import "CSOPullRequest.h"


@implementation CSORepositoryEngine

//https://api.github.com/repos/<criador>/<repositório>/pulls

+(instancetype) sharedEngine {
    static  CSORepositoryEngine *engine ;
    static dispatch_once_t onceToken;
    
    
    dispatch_once(&onceToken, ^{
        engine = [[CSORepositoryEngine alloc] init];
        engine.basePath = @"repos";
    });
    
    return engine ;
}

-(void) getPullsWith:(NSString *) ownerName andRepository:(NSString *) repositoryName withCompletion:(CSOBaseEngineCompletion) completion {
    [super makeRequestWithApiPath:[NSString stringWithFormat:@"%@/%@/%@/pulls",self.basePath,ownerName,repositoryName] requestType:RequestTypeGet parameters:nil withCompletion:^(NSArray *jsonArray, NSError * error, NSDictionary * pagination) {
        
        if (error) {
            completion(nil, error, nil);
            return ;
        }
        
        NSMutableArray * parsedItems = [NSMutableArray new];
        
        for (NSDictionary * json in jsonArray) {
            CSOPullRequest *pullRequest = [CSOPullRequest initWithJson:json];
            
            [parsedItems addObject:pullRequest];
        }
        
        completion(parsedItems, nil, nil);
    }];
}

@end
