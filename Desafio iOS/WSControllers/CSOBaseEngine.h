//
//  CSOBaseEngine.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CSOBaseEngine : NSObject

typedef NS_ENUM(NSInteger, RequestType) {
    RequestTypeGet,
    RequestTypePost
};


typedef void (^CSOBaseEngineCompletion)(id,NSError *, NSDictionary *);


@property (strong, nonatomic) NSString * basePath ;

-(void) makeRequestWithApiPath:(NSString *) path requestType:(RequestType) requestType parameters:(NSDictionary *) parameters withCompletion:(CSOBaseEngineCompletion) completion ;

@end
