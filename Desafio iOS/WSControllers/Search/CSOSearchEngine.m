//
//  CSOSearchEngine.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOSearchEngine.h"
#import "CSORepository.h"

@implementation CSOSearchEngine

#pragma mark - Singleton
+(instancetype) sharedEngine {
    static  CSOSearchEngine *engine ;
    static dispatch_once_t onceToken;
    
    
    dispatch_once(&onceToken, ^{
        engine = [[CSOSearchEngine alloc] init];
        engine.basePath = @"search";
    });
    
    return engine ;
}

#pragma mark - Communication
-(void) searchMostPopularJavaRepositoriesInPage:(int) pageNumber withCompletion:(CSOBaseEngineCompletion) completion {
    [super makeRequestWithApiPath:[self.basePath stringByAppendingString:@"/repositories"] requestType:RequestTypeGet parameters:@{@"q":@"language:Java",@"sort":@"stars",@"page":@(pageNumber)} withCompletion:^(NSDictionary * responseJson, NSError * error, NSDictionary * pagination) {
        
        if (error) {
            completion(nil, error, nil);
            return ;
        }
        
        NSArray * items = responseJson[@"items"];
        NSMutableArray * parsedItems = [NSMutableArray new];
        
        for (NSDictionary * json in items) {
            CSORepository *repository = [CSORepository initWithJson:json];
            
            [parsedItems addObject:repository];
        }
        
        NSNumber * resultsCount = responseJson[@"total_count"];
        completion(parsedItems, nil, @{@"total_count":resultsCount});
    }];
}

@end
