//
//  CSOSearchEngine.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOBaseEngine.h"

@interface CSOSearchEngine : CSOBaseEngine

+(instancetype) sharedEngine ;

-(void) searchMostPopularJavaRepositoriesInPage:(int) pageNumber withCompletion:(CSOBaseEngineCompletion) completion ;

@end
