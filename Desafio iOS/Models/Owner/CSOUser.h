//
//  CSOUser.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DCParserConfiguration.h"
#import "DCObjectMapping.h"
#import "DCKeyValueObjectMapping.h"

@interface CSOUser : NSObject

@property (strong, nonatomic) NSNumber * _id ;
@property (strong, nonatomic) NSString * login ;
@property (strong, nonatomic) NSString * avatarUrl ;
@property (strong, nonatomic) NSDictionary * JSON ;

+(instancetype)initWithJson:(NSDictionary *)json ;
+(void) addMappingsToConfig:(DCParserConfiguration *) config ;

@end
