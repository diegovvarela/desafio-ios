//
//  CSOUser.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOUser.h"

@implementation CSOUser

+(instancetype)initWithJson:(NSDictionary *)json {
    DCParserConfiguration *config = [DCParserConfiguration configuration];
    
    DCObjectMapping *idChangeOwner = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"_id" onClass:[CSOUser class]];
    DCObjectMapping *avatar = [DCObjectMapping mapKeyPath:@"avatar_url" toAttribute:@"avatarUrl" onClass:[CSOUser class]];
    
    [config addObjectMapping:idChangeOwner];
    [config addObjectMapping:avatar];
    
    DCKeyValueObjectMapping * parser  = [DCKeyValueObjectMapping mapperForClass: [CSOUser class]  andConfiguration:config];
    
    
    CSOUser * user = [parser parseDictionary:json];
    user.JSON = json ;

    return user ;
}

+(void)addMappingsToConfig:(DCParserConfiguration *)config {
    DCObjectMapping *idChangeOwner = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"_id" onClass:[CSOUser class]];
    DCObjectMapping *avatar = [DCObjectMapping mapKeyPath:@"avatar_url" toAttribute:@"avatarUrl" onClass:[CSOUser class]];
    
    [config addObjectMapping:idChangeOwner];
    [config addObjectMapping:avatar];
}

@end
