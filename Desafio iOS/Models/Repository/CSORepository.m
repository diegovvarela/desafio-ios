//
//  CSORepository.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepository.h"

@implementation CSORepository

+(instancetype)initWithJson:(NSDictionary *)json {
    DCParserConfiguration *config = [DCParserConfiguration configuration];
    
    DCObjectMapping *idChangeRep = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"_id" onClass:[CSORepository class]];
    DCObjectMapping *description = [DCObjectMapping mapKeyPath:@"description" toAttribute:@"details" onClass:[CSORepository class]];
    DCObjectMapping *stars = [DCObjectMapping mapKeyPath:@"stargazers_count" toAttribute:@"stars" onClass:[CSORepository class]];
    DCObjectMapping *forks = [DCObjectMapping mapKeyPath:@"forks_count" toAttribute:@"forks" onClass:[CSORepository class]];
    
    [config addObjectMapping:idChangeRep];
    [config addObjectMapping:description];
    [config addObjectMapping:stars];
    [config addObjectMapping:forks];
    
    [CSOUser addMappingsToConfig:config];
    
    DCKeyValueObjectMapping * parser  = [DCKeyValueObjectMapping mapperForClass: [CSORepository class]  andConfiguration:config];
    
    CSORepository * repository = [parser parseDictionary:json];
    repository.JSON = json ;
    repository.owner.JSON = json[@"owner"];
    
    return repository;
}

@end
