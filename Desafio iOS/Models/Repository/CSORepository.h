//
//  CSORepository.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSOUser.h"

@interface CSORepository : NSObject

@property (strong, nonatomic) NSNumber * _id ;
@property (strong, nonatomic) NSString * name ;
@property (strong, nonatomic) NSString * details ;
@property (strong, nonatomic) NSString * url ;
@property (strong, nonatomic) NSNumber * forks ;
@property (strong, nonatomic) NSNumber * stars ;
@property (strong, nonatomic) CSOUser  * owner ;
@property (strong, nonatomic) NSDictionary * JSON ;

+(instancetype) initWithJson:(NSDictionary *) json ;

@end
