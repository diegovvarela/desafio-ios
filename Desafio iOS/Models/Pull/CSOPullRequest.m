//
//  CSOPullRequest.m
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSOPullRequest.h"

@implementation CSOPullRequest

+(instancetype)initWithJson:(NSDictionary *)json {
    DCParserConfiguration *config = [DCParserConfiguration configuration];
    
    DCObjectMapping *idChangeRep = [DCObjectMapping mapKeyPath:@"id" toAttribute:@"_id" onClass:[CSOPullRequest class]];
    DCObjectMapping *created = [DCObjectMapping mapKeyPath:@"created_at" toAttribute:@"created" onClass:[CSOPullRequest class]];
    DCObjectMapping *urlMap = [DCObjectMapping mapKeyPath:@"html_url" toAttribute:@"url" onClass:[CSOPullRequest class]];
    
    
    [config addObjectMapping:idChangeRep];
    [config addObjectMapping:created];
    [config addObjectMapping:urlMap];
    
    [CSOUser addMappingsToConfig:config];
    
    config.datePattern = @"yyyy-MM-dd'T'HH:mm:ssZ";
    
    DCKeyValueObjectMapping * parser  = [DCKeyValueObjectMapping mapperForClass: [CSOPullRequest class]  andConfiguration:config];
    
    
    CSOPullRequest * pullRequest = [parser parseDictionary:json];
    pullRequest.JSON = json ;
    pullRequest.user.JSON = json[@"user"];
    
    return pullRequest ;
}

@end
