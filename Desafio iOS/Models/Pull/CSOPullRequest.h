//
//  CSOPullRequest.h
//  Desafio iOS
//
//  Created by Diego Varela on 08/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSOUser.h"

@interface CSOPullRequest : NSObject

@property (strong, nonatomic) NSNumber * _id ;
@property (strong, nonatomic) NSString * title ;
@property (strong, nonatomic) NSString * body ;
@property (strong, nonatomic) NSDate   * created ;
@property (strong, nonatomic) NSString * url ;
@property (strong, nonatomic) CSOUser  * user ;
@property (strong, nonatomic) NSDictionary * JSON ;

+(instancetype) initWithJson:(NSDictionary *) json ;

@end
