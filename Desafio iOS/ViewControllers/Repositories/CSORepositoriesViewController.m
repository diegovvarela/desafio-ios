//
//  CSORepositoriesViewController.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepositoriesViewController.h"
#import "CSORepositoryDetailsViewController.h"
#import "CSOSearchEngine.h"
#import "CSORepositoryEngine.h"
#import "CSORepositoryCell.h"
#import "CSOLoadingCell.h"
#import "UIView+ProgressHUD.h"
#import "AppDelegate.h"
#import "UIAlertView+Errors.h"


static NSString * kDetailsSegue = @"RepositoryDetailsSegue" ;

@interface CSORepositoriesViewController () {
    BOOL isRequesting ; // Requesting state
}

@property (strong, nonatomic) NSNumber * nextPage ;
@property (strong, nonatomic) NSMutableArray * results ;

@end

@implementation CSORepositoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    
    self.clearsSelectionOnViewWillAppear = YES;
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _results = [NSMutableArray new];
    _nextPage = @1 ;
    
    [self getNextSearchPage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count + @(_nextPage != nil && _results.count > 0).intValue ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == _results.count) {
        // Loading Cell
        CSOLoadingCell * cell = [tableView dequeueReusableCellWithIdentifier:@"LoadingCell" forIndexPath:indexPath];
        [cell.indicator startAnimating];
        return cell ;
    }
    
    CSORepositoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepositoryCell" forIndexPath:indexPath];
    
    // Configure the cell...
    
    [cell fillDataWith:_results[indexPath.row]];
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == _results.count) {
        return 80 ;
    }
    return 121.f ;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == _results.count && !isRequesting) {
        [self getNextSearchPage];
    }
}

#pragma mark - API Calls
-(void) getNextSearchPage {
    
    MBProgressHUD *hud = nil ;
    
    isRequesting = YES ;
    
    if (_results.count == 0) {
        AppDelegate * delegate = (AppDelegate *) [UIApplication sharedApplication].delegate ;
        hud = [delegate.window addProgressHUDWithTitle:@"Loading..."];
    }
    
    [[CSOSearchEngine sharedEngine] searchMostPopularJavaRepositoriesInPage:_nextPage.intValue withCompletion:^(NSArray *parsedItems, NSError * error, NSDictionary * pagination) {
        
        isRequesting = NO ;
        
        if (hud) {
            [hud hideAnimated:YES];
        }
        
        if(error) {
            [UIAlertView showCustomErrorAlert];
            return ;
        }
        
        NSNumber * total = pagination[@"total_count"];
        [_results addObjectsFromArray:parsedItems];
        
        if (_results.count < total.intValue) {
            _nextPage = @(_nextPage.intValue + 1);
        }
        else {
            _nextPage = nil ;
        }
        
        [self.tableView reloadData];
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:kDetailsSegue]) {
        CSORepositoryDetailsViewController * vc = segue.destinationViewController ;
        NSIndexPath * indexPath = [self.tableView indexPathForCell:sender];
        
        vc.repository = _results[indexPath.row];
    }
}


@end
