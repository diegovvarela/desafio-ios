//
//  CSOPullRequestViewController.m
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import "CSORepositoryDetailsViewController.h"
#import "CSOPullRequestCell.h"
#import "CSORepositoryEngine.h"
#import "UIView+ProgressHUD.h"
#import "AppDelegate.h"
#import "UIAlertView+Errors.h"

@interface CSORepositoryDetailsViewController ()

@property (strong, nonatomic) NSMutableArray * results ;

@end

@implementation CSORepositoryDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.title = self.repository.name ;
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _results = [NSMutableArray new];
    
    [self getPulls];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _results.count ;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CSOPullRequestCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PullRequestCell" forIndexPath:indexPath];
    
    // Configure the cell...
    [cell fillDataWith:_results[indexPath.row]];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {    
    return 110.f ;
}

#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    CSOPullRequest * pull = _results[indexPath.row] ;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:pull.url]];
}

#pragma mark - API Calls
-(void) getPulls {
    AppDelegate * delegate = (AppDelegate *) [UIApplication sharedApplication].delegate ;
    MBProgressHUD *hud = [delegate.window addProgressHUDWithTitle:@"Loading..."];
    
    [[CSORepositoryEngine sharedEngine] getPullsWith:self.repository.owner.login andRepository:self.repository.name withCompletion:^(NSArray *parsedItems, NSError * error, NSDictionary * pagination) {
        
        [hud hideAnimated:YES];
        
        if (error) {
            [UIAlertView showCustomErrorAlert];
            return ;
        }
        
        [_results addObjectsFromArray:parsedItems];
        [self.tableView reloadData];
    }];
}

@end
