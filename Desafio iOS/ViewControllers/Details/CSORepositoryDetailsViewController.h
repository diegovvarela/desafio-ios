//
//  CSOPullRequestViewController.h
//  Desafio iOS
//
//  Created by Diego Varela on 09/10/16.
//  Copyright © 2016 Diego Varela. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CSORepository.h"

@interface CSORepositoryDetailsViewController : UITableViewController

@property (strong, nonatomic) CSORepository * repository ;

@end
